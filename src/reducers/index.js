// Chứa tất cả các reducers
// index.js là root reducers tổng hợp lại   -> store   ->
// Tương ứng 1 reducer là 1 action creators
import demoReducer from "./demoReducer";
import { combineReducers } from "redux";

const rootReducer = combineReducers({
  demo: demoReducer,
  //     demo1: demo1Reducers,
  //     demo2: demo2Reducers,
});
export default rootReducer;
